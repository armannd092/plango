// I2C lib
#include <Wire.h>
#define SDA 21
#define SCL 22
// FastLED lib
#include "FastLED.h"
#define NUM_LEDS 16
#define BRIGHTNESS  64
#define LED_TYPE    WS2811
#define COLOR_ORDER GRB
#define UPDATES_PER_SECOND 100

// Communication var

int todo[15];
byte sch[4] = { 0b0001, 0b0010, 0b0100, 0b1000};
bool check = false;

// LED var
CRGB leds[NUM_LEDS];


void setup() {
  Wire.begin();  // join i2c bus (address optional for master)
  Serial.begin(115200);  // start serial for output
  Serial.println("hello esp32");
  LEDSetup();
}

void loop() {
  BaordUpdate();
  delay(500);
}

void BaordUpdate() {

  ////////////////////////////////commuincation///////////////////////////
  byte c[8];
  int count = 0;
  int converted[16];
  if (!check) {
    Wire.requestFrom(1, 8);    // request 8 bytes from slave device #1
    Serial.println("req sent");
    if (Wire.available() > 0) {
      Serial.println("wire is available");
      if ( count < 8) {
        c[count] = Wire.read();
        Serial.println(Wire.read(), HEX);
        count++;
        Serial.println(String(count));
      }
      else {
        count = 0;
        Serial.println("new set");
      }
    }
    ConvertToid(c, converted, check);
  }

  /////////////////////////////////////////////////////////////////////////
  /////////////////////////////////LED management/////////////////////////
  LEDColor();
  ////////////////////////////////////////////////////////////////////////
}
void ConvertToid(byte chunk[8], int ids[16], bool ckeck) {
  //seperate and convert sended bytes to the ids of the schadule
  int count = 0;
  int actPin = 0;
  for (int i = 0; i < 9; i + 2) {
    for (int j = 0; j < sizeof(sch); j++) {
      if (chunk[i] >> 4 == sch[j]) {
        ids[i] = j;
        count++;
      }
      else if (chunk[i] & 0xF == sch[j]) {
        ids[i + 1] = j;
        count++;
      }
      else {
        actPin = (byte) chunk[i] & 0xF;
      }
    }
    //Serial.println(ids[i] , ids[i+1]);
  }
  if (count == actPin) {
    check = true
  }
}
void LEDSetup() {
  FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection( TypicalLEDStrip );
  FastLED.setBrightness(  BRIGHTNESS );
}
void LEDColor(int ids[16]) {
  for (int i = 0 ; i < sizeof(ids); i++) {
    if (ids[i] == 0) {
      //red
      leds[i] = CRGB(200, 0, 0);
    }
    else if (ids[i] == 1) {
      //green
      leds[i] = CRGB(0, 200, 20);
    }
    else if (ids[i] == 2) {
      //blue
      leds[i] = CRGB(0, 150, 200);
    }
    else if (ids[i] == 3) {
      //yellow
      leds[i] = CRGB(200, 200, 0);
    }
  }
  FastLED.show();
  FastLED.delay(1000 / UPDATES_PER_SECOND);

}
