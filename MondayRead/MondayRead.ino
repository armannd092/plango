
#include <Wire.h>
#define SDA 8
#define SCL 9

int address = 1;
int pin[15] = {2};
int todo[4] = {100, 200, 300, 400};
byte tosend[8];
int count = 0;



void setup() {
  Serial.swap(0);
  Serial.begin(115200);
  Wire.begin(address);
  Wire.onRequest(requestEvent);
  for (int i = 0; i < 16; i++) {
    pinMode(pin[i], OUTPUT);
  }
  Serial.print("pin defined");
}
void loop() {
  for (int i = 0; i < 16; i++) {
    byte sch = PinRead(i);
    Serial.println(sch);
    byte buff;
    if (sch > 0) {
      count++;
    }
    if ( i % 2 == 0) {
      buff = sch << 4;
      if (i == 14) {
        byte br = (int)count;
        tosend[(i - 1) / 2] = buff + br;
      }
    }
    else if(i%2 != 0) {
      tosend[(i - 1) / 2] = buff + sch;
    }
  }

}

void requestEvent() {
  Wire.beginTransmission(address);
  for (int i = 0 ; i < 8 ; i++) {
    Wire.write((byte*)&tosend[i] , 8);
  }
  Wire.endTransmission();
}

byte  PinRead(int id) {
  // read the voltage form pin and map the value to the function
  byte sch;
  int vol = analogRead(pin[id]);
  if (0 < vol <= todo[0]) {
    sch = 0b0001;
  }
  else if (todo[0] < vol <= todo[1]) {
    sch = 0b0010;
  }
  else if ( todo[1] < vol <= todo[2]) {
    sch = 0b0100;
  }
  else if (todo[2] < vol <= todo[3]) {
    sch = 0b1000;
  }
  return sch;
}
